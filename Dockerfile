FROM adoptopenjdk:11.0.6_10-jre-hotspot
RUN mkdir /opt/app
COPY ./target/*.jar /opt/app/app.jar
CMD ["sh","-c","java $EXTRA_ARGS -jar /opt/app/app.jar"]
