package pl.trmewobrot.websockets.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.trmewobrot.websockets.model.Stock;
import pl.trmewobrot.websockets.service.StockService;

import java.util.List;

@Controller
@Slf4j
@RequestMapping("/stocks")
@AllArgsConstructor
class StockController {
    private final StockService stockService;

    @GetMapping
    @ResponseBody
    List<Stock> getAllStocks(){
        return stockService.getStocks();
    }

    //stomp
    @MessageMapping("/start")
    public void start(StompHeaderAccessor stompHeaderAccessor) {
        log.info(stompHeaderAccessor.getSessionId());
        stockService.addSession(stompHeaderAccessor.getSessionId());
    }
    @MessageMapping("/stop")
    public void stop(StompHeaderAccessor stompHeaderAccessor) {
        stockService.removeSession(stompHeaderAccessor.getSessionId());
    }

}
