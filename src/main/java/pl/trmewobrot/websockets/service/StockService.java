package pl.trmewobrot.websockets.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import pl.trmewobrot.websockets.model.Stock;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
@Slf4j
public class StockService {
    private final Set<String> listenerClients = new HashSet<>();
    private final List<Stock> stock=new LinkedList<>();

    @PostConstruct
    private void putData(){
        stock.add(new Stock("Alior",38.80,16.6650,16.6300));
        stock.add(new Stock("CCC",368.80,16.6650,17.6300));
        stock.add(new Stock("CDPROJECT",368.80,16.6650,145.6300));
        stock.add(new Stock("CyfrPLSat",368.80,16.6650,1644.6300));
        stock.add(new Stock("DINOPL",68.80,13.6650,16.6300));
        stock.add(new Stock("JSW",332.80,16.6650,10.6300));
        stock.add(new Stock("KGHM",368.80,16.6650,16.6300));
        stock.add(new Stock("LOTOS",38.80,14.6650,16.6300));
        stock.add(new Stock("LPP",368.80,16.6650,16.6300));
        stock.add(new Stock("MBANK",368.80,16.6650,16.6300));
        stock.add(new Stock("ORANGEPL",368.80,16.6650,1.6300));
        stock.add(new Stock("PEKAO",368.80,16.6650,156.6300));
        stock.add(new Stock("PGE",368.80,16.6650,23.6300));
        stock.add(new Stock("PGNIG",368.80,16.6650,36.6300));
        stock.add(new Stock("PKNORLEN",368.80,16.6650,16.6300));
        stock.add(new Stock("Sample",368.80,16.6650,16.6300));
        stock.add(new Stock("Sample 2",368.80,16.6650,16.6300));
        stock.add(new Stock("Sample 3",368.80,16.6650,16.6300));
        stock.add(new Stock("Sample 4",368.80,16.6650,16.6300));
        stock.add(new Stock("Sample 5",368.80,16.6650,16.6300));
    }

    @EventListener
    public void sessionDisconnectionHandler(SessionDisconnectEvent event) {
        String sessionId = event.getSessionId();
        log.info("Disconnecting " + sessionId + "!");
        removeSession(sessionId);
    }

    public List<Stock> getStocks(){
        return stock;
    }

    public void addSession(String sessionId) {
        log.info("Added session "+sessionId);
        this.listenerClients.add(sessionId);
    }

    public void removeSession(String sessionId) {
        log.info("Removed session "+sessionId);
        this.listenerClients.remove(sessionId);
    }

    public Set<String> getListenerClients() {
        return listenerClients;
    }

    public Stock getRandomChangedStock() {
        Random rand= new Random();
        int index = rand.nextInt(stock.size());
        Stock stockObj=stock.get(index);
        int randChoice=rand.nextInt(3)+1;
        double randDouble=Math.round((rand.nextDouble()*400)+1);
        switch(randChoice){
            case 1:
                stockObj.setPurchaseLimit(randDouble);
                break;
            case 2:
                stockObj.setSaleLimit(randDouble);
                break;
            case 3:
                stockObj.setRate(randDouble);
                break;
        }
        return stockObj;
    }
}
