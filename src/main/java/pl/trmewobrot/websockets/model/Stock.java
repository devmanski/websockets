package pl.trmewobrot.websockets.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Stock {
    public String name;
    public Double purchaseLimit;
    public Double saleLimit;
    public Double rate;
}
