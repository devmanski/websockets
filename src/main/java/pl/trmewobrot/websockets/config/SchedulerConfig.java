package pl.trmewobrot.websockets.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import pl.trmewobrot.websockets.model.Stock;
import pl.trmewobrot.websockets.service.StockService;

import javax.annotation.PostConstruct;
import javax.management.Notification;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Configuration
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
public class SchedulerConfig {
    private final SimpMessagingTemplate template;
    private final StockService stockService;

    @Scheduled(fixedDelay = 6000)
    public void dispatch(){
        for (String listener : stockService.getListenerClients()) {
            SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
            headerAccessor.setSessionId(listener);
            headerAccessor.setLeaveMutable(true);
            template.convertAndSendToUser(
                    listener,
                    "/topic/stocks",
                    stockService.getRandomChangedStock(),
                    headerAccessor.getMessageHeaders());
        }
    }
}
